// import config from '@/config'
// import helpers from '@/plugins/helpers'
import { db } from '@/plugins/firebase'

const initialize = (context) => {
  return new Promise(async (resolve, reject) => {
    try {
      // if (process.env.NODE_ENV === 'development') {
      // context.commit('SET_USER', config.devUser)
      // } else {
      // const userProfile = await helpers.fetchApi(config.apiUrl + 'authentication/user', 'get')
      // context.commit('SET_USER', userProfile)
      // config.env === 'dev' ? console.log(userProfile) : null
      // }
      const localWallets = []

      // await db.collection('wallets').onSnapshot((snapshot) => {
      //   snapshot.forEach((doc) => {
      //     if (doc.id !== '_schema') {
      //       const wallet = doc.data()
      //       localWallets.push(wallet)
      //     }
      //   })
      //   context.commit('SET_WALLETS', localWallets)
      //   resolve()
      // })
      context.commit('SET_WALLETS', localWallets)
      resolve()
      // console.log(wallets)
    } catch (err) {
      reject(err)
    }
  })
}

const setSnackbar = (context, data) => {
  context.commit('SET_SNACKBAR', data)
}
const setLoading = (context, data) => {
  context.commit('SET_LOADING', data)
}

export { initialize, setSnackbar, setLoading }
