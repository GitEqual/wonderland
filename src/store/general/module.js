const SET_SNACKBAR = 'SET_SNACKBAR'
const SET_LOADING = 'SET_LOADING'
// const SET_USER = 'SET_USER'
// const SET_SUPPORTMAIL = 'SET_SUPPORTMAIL'
// -----------------------------------------
const SET_WALLETS = 'SET_WALLETS'

const state = {
  snackmsg: '',
  snackstatus: '',
  loading: {
    state: false,
    msg: '',
  },
  user: {},
  supportEmail: '',
  wallets: [],
}

const getters = {
  snackmsg: (state) => state.snackmsg,
  snackstatus: (state) => state.snackstatus,
  wallets: (state) => state.wallets,
  loading: (state) => state.loading,
  // user: (state) => state.user,
  // supportEmail: (state) => state.supportEmail,
}

const mutations = {
  [SET_SNACKBAR]: (state, data) => {
    state.snackmsg = data.msg
    state.snackstatus = data.status
  },
  [SET_WALLETS]: (state, data) => {
    // console.log(data)

    state.wallets = data
  },
  [SET_LOADING]: (state, data) => {
    state.loading = data
  },
  // [SET_USER]: (state, data) => {
  //   state.user = data
  // },
  // [SET_SUPPORTMAIL]: (state, data) => {
  //   state.supportEmail = data
  // },
}

export { state, getters, mutations }
