const SET_EXAMPLE = 'SET_EXAMPLE'

const state = {
  example: false
}

const getters = {
  example: state => state.example
}

const mutations = {
  [SET_EXAMPLE]: (state, data) => {
    state.example = data
  }
}

export {
  state,
  getters,
  mutations
}
