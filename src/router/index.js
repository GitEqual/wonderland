import Vue from 'vue'
import Router from 'vue-router'
import { auth } from '@/plugins/firebase'

Vue.use(Router)
let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      redirect: '/',
    },
    {
      path: '/',
      name: 'Wonderland',
      meta: { layout: 'default', requiresAuth: false },
      component: () => import('@/pages/wonderland/wonderland'),
    },
  ],
})

router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some((x) => x.meta.requiresAuth)
  if (requiresAuth && !auth.currentUser) {
    next('/')
  } else {
    next()
  }
})

export default router
