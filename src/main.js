import Vue from 'vue'
import App from '@/app/app.vue'
import router from '@/router'
import store from '@/store'
import { auth } from '@/plugins/firebase'
// import * as fb from '@/plugins/firebase'
// import './registerServiceWorker'
import '@babel/polyfill'

import helpers from '@/plugins/helpers'
Vue.prototype.$helpers = helpers
import config from '@/config'
Vue.prototype.$config = config

// plugins
import vuetify from '@/plugins/vuetify'
import VueDateFns from 'vue-date-fns'
Vue.filter('date', VueDateFns.dateFilter)
Vue.use(VueDateFns)

import JsonViewer from 'vue-json-viewer'
Vue.use(JsonViewer)

import VueCurrencyFilter from 'vue-currency-filter'
Vue.use(VueCurrencyFilter, {
  symbol: '$',
  thousandsSeparator: '.',
  fractionCount: 2,
  fractionSeparator: ',',
  symbolPosition: 'front',
  symbolSpacing: true,
  avoidEmptyDecimals: undefined,
})

import LayoutDefault from '@/layouts/default'
import LayoutEmpty from '@/layouts/empty.vue'

Vue.component('default-layout', LayoutDefault)
Vue.component('empty-layout', LayoutEmpty)

import cCard from '@/layouts/components/card.vue'
Vue.component('ccard', cCard)
// import DataTable from '@/components/data-table.vue'
// Vue.component('data-table', DataTable)

// import RmText from '@/components/inputs/text-field'
// Vue.component('rm-text', RmText)

// Style
import '@/style/style.scss'

Vue.config.productionTip = false

// new Vue({
//   router,
//   store,
//   vuetify,
//   render: h => h(App)
// }).$mount('#app')

let app
auth.onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      vuetify,
      // VueAnimXYZ,
      render: (h) => h(App),
    }).$mount('#app')
  }
})
