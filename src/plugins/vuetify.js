import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import en from 'vuetify/es5/locale/en'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      dark: {
        background: '#192837',
        appbar: '#192837',
        background1: '#192837',
        primary: '#424242',
        secondary: '#ff5722',
        accent: '#ffeb3b',
        error: '#f44336',
        warning: '#ffc107',
        info: '#607d8b',
        success: '#4caf50',
      },
      light: {
        background: '#192837',
        appbar: '#192837',
        background1: '#192837',
        primary: '#424242',
        secondary: '#ff5722',
        accent: '#ffeb3b',
        error: '#f44336',
        warning: '#ffc107',
        info: '#607d8b',
        success: '#4caf50',
      },
    },
  },
  lang: {
    locales: { en },
    current: 'en',
  },
  icons: {
    iconfont: 'mdi',
  },
})
