import $store from '../store'

const fetchApi = async (url = '', method = '', data = {}, headers = {}) => {
  return new Promise(async (resolve, reject) => {
    // let _csrf =
    if (url == '') {
      console.error('Cannot fetch without URL')
      return
    }
    method = method.toUpperCase()
    if (method == '') {
      method = 'GET'
    }
    if (method != 'GET' && method != 'POST' && method != 'PUT' && method != 'DELETE') {
      console.error('Invalid method, please use: GET, POST, PUT, DELETE ')
      return
    }
    try {
      let response
      if (method == 'GET') {
        response = await fetch(url, { method: method }).then(async (resp) => {
          let responseJson = await resp.json()
          responseJson.statusCode = resp.status
          return responseJson
        })
      }
      if (method == 'POST' || method == 'PUT' || method == 'DELETE') {
        data._csrf = $store.state.general.user.csrf
        let accept = { Accept: 'application/json', 'Content-Type': 'application/json' }
        Object.assign(headers, accept)
        response = await fetch(url, {
          headers: headers,
          method: method,
          body: JSON.stringify(data),
        }).then(async (resp) => {
          let responseJson = await resp.json()
          responseJson.statusCode = resp.status
          return responseJson
        })
      }
      resolve(response)
    } catch (err) {
      reject(err)
    }
  })
}

const fetchGate = async () => {
  return new Promise(async (resolve, reject) => {
    try {
      resolve()
    } catch (error) {
      reject(error)
    }
  })
}

export default { fetchApi, fetchGate }
