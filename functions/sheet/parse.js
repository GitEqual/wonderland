const admin = require('firebase-admin')
const db = admin.firestore()

const pushToDb = async (doc, data) => {
  // await db
  //   .collection('sheet')
  //   .doc(doc)
  //   .delete()
  //   .then(() => {
  //     console.log('Document successfully deleted!')
  //   })
  //   .catch((error) => {
  //     console.error('Error removing document: ', error)
  //   })
  await db
    .collection('sheet')
    .doc(doc)
    .set({
      raw: data,
    })
    .then(() => {
      return
      // return res.status(200).json({
      //   message: 'Data updated',
      // })
    })
    .catch((error) => {
      return error
    })
  return
}

exports.sheet = async (results, res) => {
  // Create first project
  return new Promise(async (resolve, reject) => {
    try {
      //   console.log(results[0])
      let marketCap = {
        name: 'Market Cap',
        prefix: '$',
        suffix: '',
        opposite: true,
        values: [],
        color: '#FFA600',
      }
      let tvl = {
        name: 'TVL',
        prefix: '$',
        suffix: '',
        opposite: true,
        values: [],
        color: '#FF6361',
      }
      let treasury = {
        name: 'Treasury',
        prefix: '$',
        suffix: '',
        opposite: true,
        values: [],
        color: '#40E6DA',
      }
      let runway = {
        name: 'Runway',
        prefix: '',
        suffix: ' days',
        opposite: false,
        values: [],
        color: '#BC5090',
      }
      let price = {
        name: '$TIME',
        prefix: '$',
        suffix: '',
        opposite: true,
        values: [],
        color: '#40E6DA',
      }
      let backing = {
        name: 'Backing per $TIME',
        prefix: '$',
        suffix: '',
        opposite: true,
        values: [],
        color: '#FFA600',
      }
      let apy = {
        name: 'APY',
        prefix: '',
        suffix: '%',
        opposite: false,
        values: [],
        color: '#BC5090',
      }
      let supply = {
        name: 'Supply',
        prefix: '',
        suffix: '',
        opposite: true,
        values: [],
        color: '#40E6DA',
      }
      let staked = {
        name: 'Staked',
        prefix: '',
        suffix: '%',
        opposite: false,
        values: [],
        color: '#FFA600',
      }
      let wallets = {
        name: 'Wallets',
        prefix: '',
        suffix: '',
        opposite: true,
        values: [],
        color: '#BC5090',
      }
      let time = {
        name: '$TIME%',
        prefix: '',
        suffix: '%',
        opposite: false,
        values: [],
        color: '#40E6DA',
      }
      let memo = {
        name: '$MEMO%',
        prefix: '',
        suffix: '%',
        opposite: false,
        values: [],
        color: '#FFA600',
      }
      let wmemo = {
        name: '$wMEMO%',
        prefix: '',
        suffix: '%',
        opposite: false,
        values: [],
        color: '#FF6361',
      }
      let wmemoPrice = {
        name: '$wMEMO',
        prefix: '$',
        suffix: '',
        opposite: true,
        values: [],
        color: '#FF6361',
      }
      for (let v = 0; v < results.length; v++) {
        if (v === 0) continue
        const value = results[v]

        if (value[2] === '') continue
        let localDate = ''
        // // console.log(value[0])
        Date.prototype.addDays = function (days) {
          var date = new Date(this.valueOf())
          date.setDate(date.getDate() + days)
          return date
        }

        localDate = new Date(results[1][0]).addDays(v - 1).getTime()
        marketCap.values.push({
          date: localDate,
          value: value[3],
        })
        tvl.values.push({
          date: localDate,
          value: value[4],
        })
        wmemoPrice.values.push({
          date: localDate,
          value: value[2],
        })
        treasury.values.push({
          date: localDate,
          value: value[7],
        })
        price.values.push({
          date: localDate,
          value: value[1],
        })
        backing.values.push({
          date: localDate,
          value: value[8],
        })
        apy.values.push({
          date: localDate,
          value: value[5] * 100,
        })
        runway.values.push({
          date: localDate,
          value: value[9],
        })
        let sVsupply = value[29]
        sVsupply = sVsupply.toFixed(0)
        supply.values.push({
          date: localDate,
          value: Number(sVsupply),
        })
        if (value[32]) {
          wallets.values.push({
            date: localDate,
            value: value[32],
          })
        }
        if (value[40]) {
          let sV = value[40] * 100
          sV = sV.toFixed(2)
          staked.values.push({
            date: localDate,
            value: Number(sV),
          })
        }
        if (value[35]) {
          let tV = value[35] * 100
          tV = tV.toFixed(2)
          time.values.push({
            date: localDate,
            value: Number(tV),
          })
        }
        if (value[37]) {
          let mV = value[37] * 100
          mV = mV.toFixed(2)
          memo.values.push({
            date: localDate,
            value: Number(mV),
          })
        }
        if (value[39]) {
          let wV = value[39] * 100
          wV = wV.toFixed(2)
          wmemo.values.push({
            date: localDate,
            value: Number(wV),
          })
        }
      }
      await pushToDb('marketCap', marketCap)
      await pushToDb('tvl', tvl)
      await pushToDb('treasury', treasury)
      await pushToDb('runway', runway)
      await pushToDb('timePrice', price)
      await pushToDb('backing', backing)
      await pushToDb('apy', apy)
      await pushToDb('supply', supply)
      await pushToDb('staked', staked)
      await pushToDb('wallets', wallets)
      await pushToDb('timePerc', time)
      await pushToDb('memoPerc', memo)
      await pushToDb('wmemoPerc', wmemo)
      await pushToDb('wmemoPrice', wmemoPrice)
      return res.status(200).json({
        message: 'Data updated',
      })
    } catch (error) {
      return res.status(500).json(error)
    }
  })
}
