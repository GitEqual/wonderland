const functions = require('firebase-functions')

const express = require('express')
const app = express()

const cors = require('cors')({ origin: true })
const cookieParser = require('cookie-parser')()

const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args))

app.use(cors)
app.use(cookieParser)
// app.use(validations.validateFirebaseIdToken)

app.get('/', async (req, res) => {
  const results = await fetch(
    'https://script.google.com/macros/s/AKfycbyj-NnQ-Drgs7DiSFYHOHLcDCuW2UUPOxDyhNJpzIb_KZXT9Xj4LeYLdneDDIfesILP/exec',
    {
      method: 'GET',
    }
  )
    .then((response) => {
      return response.text()
    })
    .then(async (result) => {
      return JSON.parse(result)[0].values
    })
    .catch((error) => {
      return res.status(500).json(error)
    })
  const parseResult = require('../sheet/parse')
  await parseResult.sheet(results, res)

  // Finished
})

module.exports = functions.region('europe-west3').https.onRequest(app)
