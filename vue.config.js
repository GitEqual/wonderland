// const path = require('path');
// const inlineLimit = 10000;

process.env.VUE_APP_NAME = require('./package.json').publicName

module.exports = {
  filenameHashing: false,
  chainWebpack: (config) => {
    config.plugin('prefetch').tap((args) => {
      return [
        {
          rel: 'prefetch',
          include: 'asyncChunks',
          fileBlacklist: [
            // /\.map$/,
            // /pdfmake\.[^.]+\.js$/,
            // /xlsx\.[^.]+\.js$/,
            // /fabric[^.]*\.[^.]+\.js$/,
            // /responsivedefaults\.[^.]+\.js$/
          ],
        },
      ]
    })
    // [
    //   {
    //     test: /\.s(c|a)ss$/,
    //     use: [
    //       'vue-style-loader',
    //       'css-loader',
    //       {
    //         loader: 'sass-loader',
    //         // Requires sass-loader@^7.0.0
    //         options: {
    //           implementation: require('sass'),
    //           fiber: require('fibers'),
    //           indentedSyntax: true // optional
    //         }
    //       }
    //     ]
    //   }
    // ]
    // config.module.rule('images').use('url-loader')
    //   .loader('file-loader') // replaces the url-loader
    //   .tap(options => Object.assign(options, {
    //     name: '[name].[ext]'
    //   }))
    // config.module.rule('svg').use('file-loader')
    //   .tap(options => Object.assign(options, {
    //     name: '[name].[ext]'
    //   }))
  },
  css: {
    extract: false,
    // extract: {
    //   filename: '[name].css',
    //   chunkFilename: '[name].css'
    // }
  },
  configureWebpack: {
    optimization: {
      splitChunks: false,
    },
    output: {
      filename: '[name].js',
      chunkFilename: '[name].js',
    },
  },
}
